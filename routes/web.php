<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('home', 'home');
Route::view('/', 'auth.register');

Route::get('contact', 'ContactFormController@create');
Route::post('contact', 'ContactFormController@store');

Route::view('about', 'about');

//CRUD routes Events>>>

Route::get('/events', 'EventsController@index');
Route::get('/events/create', 'EventsController@create');
Route::post('/events', 'EventsController@store');
Route::get('/events/{event}', 'EventsController@show'); //varible here has to be the same as the varible in eventcontoroller -- public function show(event $event);
Route::get('/events/{event}/edit', 'EventsController@edit');
Route::patch('/events/{event}', 'EventsController@update');
Route::delete('/events/{event}', 'EventsController@destroy');


//CRUD routes Users>>>

Route::get('/users', 'UserController@index');
Route::get('/register', 'UserController@create');
Route::post('/users', 'UserController@store');
Route::get('/users/{user}', 'UserController@show'); 
Route::get('/users/{user}/edit', 'UserController@edit');
Route::post('/users/{user}/update', 'UserController@update');
Route::delete('/uers/{user}', 'UserController@destroy');










Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
