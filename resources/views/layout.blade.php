<!DOCTYPE html>
<html>

<head>


	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, inital-scale=1">

	<title>@yield('title', 'Juncture')</title>


            <link href="{{ asset('css/app.css') }}"" rel="stylesheet">


</head>


<body>

<div class="container">

      	@include('nav')



      	<!--Flash message when email is sent-->
      	@if(session()->has('message'))
      		<div class="alert alert-success" role="alert">
      			<strong>Success</strong> {{ session()->get('message') }}
      		</div>
      	@endif


		@yield('content')

</div>


		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</body>
</html>