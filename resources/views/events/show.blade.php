	

@extends('layouts.app')

@section('title', 'Details for ' . $event->name)

@section('content')




<div class="row"> 
	<div class="col-12">

		<h1>Details for {{ $event->name }}</h1>
		<p><a href="/events/{{ $event->id }}/edit">Edit</a></p>

	</div>
</div>


<div class="row">
	<div class="col-12">

		<p><strong>Creator</strong> {{ $event->creator->name }}</p>
		<p><strong>Name</strong> {{ $event->name }}</p>
		<p><strong>Location</strong> {{ $event->location }}</p>		
		<p><strong>Status</strong> {{ $event->active }}</p>
		<p><strong>Date</strong> {{ $event->date }}</p>

	</div>
</div>

<form action="/events/{{ $event->id }}" method="POST">
		@method('DELETE')
		@csrf

		<button class="btn btn-danger" type="submit">Delete</button>
</form>

	
	
@endsection