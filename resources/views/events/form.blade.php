		




		<div class="form-group">

			<label for="name">Name</label>

				<input type="text" name="name" value="{{ old('name') ?? $event->name }}" class="form-control">
				<input type="hidden" value="{{Auth::user()->id }}" name="creator_id">

			<div>{{ $errors->first('name') }}</div>

		</div>


		<div class="form-group">

			<label for="email">Location</label>

				<input type="text" name="location" value="{{ old('location') ?? $event->location }}" class="form-control">
				<div>{{ $errors->first('location') }}</div>

		</div>

		<div class="form-group">

				<label for="date">Date</label>
				<input type="date" name="date" class="form-control" value="{{ old('date') ?? $event->date }}">

		</div>

		<div class="form-group">

				<label for="active">Event Status</label>

				<select name="active" id="active" class="form-control">
				
				<option value="" disabled>Select event status</option>

			@foreach($event->activeOptions() as $activeOptionKey => $activeOptionValue)

				<option value="{{ $activeOptionKey }}" {{ $event->active == $activeOptionValue ? 'selected' : '' }}>{{ $activeOptionValue }}</option>

			@endforeach	
				
				</select>

		</div>

		@csrf



