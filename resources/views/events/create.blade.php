
@extends('layouts.app')

@section('title', 'Add New Event')

@section('content')

<div class="row">
	<div class="col-12">

<h1>Add New Events</h1>

	</div>
</div>


<div class="row">
	<div class="col-12">

	<form action="/events" method="POST">

		@include('events.form')

		<button type="submit" class="btn btn-primary">Add Event</button>


	</form>

	</div>
</div>

	
	<hr>

	
@endsection