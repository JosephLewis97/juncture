
@extends('layouts.app')

@section('title', 'events')

@section('content')

   <style>
        header {
            height: 1px;
        }

        body {
            margin: 0;
        }

        .box {
            height: 120px;
            margin-top: 5px;
            margin-bottom: 30px;
            background-color:;
            overflow-x: scroll;
            overflow-y: hidden;
            white-space: nowrap;
        }

        .box::-webkit-scrollbar {
            display: none;

        }

        .card-a {
            display: inline-block;
            height: 100%;
            width: 120px;
            object-fit: fill;
        }

        .card-b {
            display: inline-block;
            background-color: yellow;
            height: 100%;
            width: 120px;
        }

        .img {
            height: 120px;
            width: 120px;
        }

    </style>

    <br>

    <div class="box">

      <!--Profile Images need to go in the image src-->
        
        <div class="card-a">
            <img src="{{ asset('profile-1-01.png') }}" ; width="120px" ; height="120px" >
        </div>

        <div class="card-a">
            <img src="{{ asset('profile-2-01.png') }}" ; width="120px" ; height="120px">
        </div>

        <div class="card-a">
            <img src="{{ asset('profile-3-01.png') }}" ; width="120px" ; height="120px">
        </div>

        <div class="card-a">
            <img src="{{ asset('profile-4-01.png') }}" ; width="120px" ; height="120px">
        </div>

        <div class="card-a">
            <img src="{{ asset('profile-1-01.png') }}" ; width="120px" ; height="120px">
        </div>

        <div class="card-a">
            <img src="{{ asset('profile-2-01.png') }}" ; width="120px" ; height="120px">
        </div>

        <div class="card-a">
            <img src="{{ asset('profile-3-01.png') }}" ; width="120px" ; height="120px">
        </div>

        <div class="card-a">
            <img src="{{ asset('profile-4-01.png') }}" ; width="120px" ; height="120px">
        </div>

        <div class="card-a">
            <img src="{{ asset('profile-1-01.png') }}" ; width="120px" ; height="120px">
        </div>

        <div class="card-a">
            <img src="{{ asset('profile-2-01.png') }}" ; width="120px" ; height="120px">
        </div>

        <div class="card-a">
            <img src="{{ asset('profile-3-01.png') }}" ; width="120px" ; height="120px">
        </div>

        <div class="card-a">
            <img src="{{ asset('profile-4-01.png') }}" ; width="120px" ; height="120px">
        </div>

    </div>

    <hr>

	<div class="row">
		<div class="col-12">

      <!--Needed to set up a new login for a company-->

			<h2>Company Sponsored Events</h2>

		</div>
	</div>

	<div class="container-fluid" style="margin-top: 20px">


        <div class="row">

            <div class="col-lg-6 col-sm-6 col-xs-6">


            <button style="

              background: linear-gradient(-45deg, #84BCDA, #849ADA); 
              font-family: 'Poppins';
              font-weight: 400;
              width: 100%;
              letter-spacing: 0.5px;
              transition: all 0.3s ease;
              cursor: pointer;
              border-radius: 6px;
              background-size: 400% 400%;
              color: #FFF;
              text-transform: uppercase;
              border: 0;
              padding: 13px 30px;
              position: relative;
              font-size: 15px;
              animation: Gradient 5s ease infinite;" type="button" class="btn btn-primary btn-lg container-fluid">
                    <img src="{{ asset('nandos.png') }}" class="img-fluid" alt="Responsive image"><br><br>

                    <!--Company event info here-->

                    <p><strong>Nandos</strong>, Friars Walk</p>
                    <p>Friday 18th October<p>
                    <p> 2-4-1 On All Main Meals</p>

                </button>

            </div>

            <div class="col-lg-6 col-sm-6 col-xs-6">


            <button style="

              background: linear-gradient(-45deg, #F49739, #ECAA20); 
              font-family: 'Poppins';
              font-weight: 400;
              width: 100%;
              letter-spacing: 0.5px;
              transition: all 0.3s ease;
              cursor: pointer;
              border-radius: 6px;
              background-size: 400% 400%;
              color: #FFF;
              text-transform: uppercase;
              border: 0;
              padding: 13px 30px;
              position: relative;
              font-size: 15px;
              animation: Gradient 5s ease infinite;" type="button" class="btn btn-primary btn-lg container-fluid"><br>

                    <!-- img will be related to the type of event, each event will have a specific icon -->

                    <img src="{{ asset('wagamama.png') }}" class="img-fluid" alt="Responsive image">
                    <br><br>

                     <p><strong>Wagamama</strong>, Friars Walk</p>
                    <p>Wednesday 16th October<p>
                    <p>30% Off All Sides</p>

                    <!-- all data for the buttons will be pulled from the database. The info will be supplied when the event is created. -->

                </button>

            </div>

        </div>

    </div>

    <br>
    <hr>



    <div class="row" style="margin-top: 20px">

	 	<div class="col-lg-6">

	 		<h2>Your Upcoming Events</h2>

	 		<p><a href="events/create">Add</a></p>

	 	</div>

	 </div>

	
	<div class="col-12">

		 	@include('events.calendar')

		 </div>

     <br>
     <hr>


	 <div class="row">

		@foreach ($events as $event)

	<div class="alert alert-primary col-12" style="margin: 10px;" role="alert">

			<div class="row">

			<div class="col-12">
				
				Who: {{ $event->creator->name }}

			</div>

			<div class="col-12">
				<a href="/events/{{ $event->id }}">What: {{ $event->name }}</a>
			</div>
			<div class="col-12">Where: {{ $event->location }}</div>
			<div class="col-12">Why: {{ $event->active }}</div>
			<div class="col-12">Date: {{ $event->date }}</div>

			</div>

	</div>

	@endforeach

		</div>

	</div>
	







@endsection