        
<style>
        .cal-container {
            font-family: sans-serif;
            text-align: center;
            margin top: 10px;

        }

        .cal-style-container {
            border-radius: 15px;
            background: white;
            padding-left: 20px;
            padding-right: 20px;
            padding-bottom: 20px;
            padding-top: 3px;
            margin-left: 15px;
            margin-top: 15px;
            position: center;
            width: 92.5%;
        }

        th {
            height: 40px;
            text-align: center;
            font-weight: 700;

        }

        td {
            height: 50px;
        }

        .today {
            background-color: orange;
        }

        th:nth-of-type(7),
        td:nth-of-type(7) {
            color: #ECA72C;
        }

        th:nth-of-type(1),
        td:nth-of-type(1) {
            color: #84BCDA;
        }

    </style>


        <?php

        date_default_timezone_set('Europe/London');

        //Get previous year and month

        if(isset($_GET['ym'])) {
            $ym = $_GET['ym'];

        //This month 
        } else {
            $ym = date('ym');
        }

        $timestamp = strtotime($ym, "-01");

        if ($timestamp == false){
            $timestamp = $time();   
        }

        //Today

        $today = date('Y-m-d', time());

        // Year and month (Title)

        $html_title = date('Y / m', $timestamp);

        // Create Previous and next month

        $prev = date('Y-m', mktime(0, 0 , 0, date('m', $timestamp), 1, date('Y'))); $timestamp;

        $next = date('Y-m', mktime(0, 0 , 0, date('m', $timestamp)+1, 1, date('Y'))); $timestamp;

        //Days in a month

        $day_count = date('t', $timestamp);

        //0:Sunday 1:Monday 2:Tuesday ......

        $str = date('w', mktime(0, 0 , 0, date('m', $timestamp)-1, 1, date('Y')));
        $timestamp;

        //Create calendar

        $weeks = array();
        $week = '';

        //Add empty cell

        $week .= str_repeat('<td></td>', $str);

        for ( $day = 1; $day <= $day_count; $day++, $str++){
            $date = $ym. '-' .$day;

            if ($today == $date){
                $week .= '<td class="today">' .$day;
            } else {
                $week .='<td>' .$day;
            }

            $week .='</td>';

            //End of week OR end of month

            if ($str % 7 == 6 || $day == $day_count){

                if($day == $day_count){
                    //Add an empty cell
                    $week .=str_repeat('<td></td>', 6 - ($str % 7));
                    }

                $weeks[] = '<tr>' .$week. '</tr>';

                //Prepare for a new week

                $week = ''; 
            }
        }

    ?>

        <!--html for calendar-->
        <div class="cal-style-container">

            <div class="cal-container">

                <h3><a href="?ym=c <?php echo $prev; ?>">&lt;</a> <?php echo $html_title; ?> <a href="?ym= <?php echo $next; ?> ">&gt;</a></h3>
                <br>

                <table class="table table-bordered">
                    <thead style="background-color: #32292F">
                        <tr style="color: white">
                            <th>S</th>
                            <th>M</th>
                            <th>T</th>
                            <th>W</th>
                            <th>T</th>
                            <th>F</th>
                            <th>S</th>
                        </tr>
                    </thead>

                    <?php
            
            foreach ($weeks as $week){
                echo $week;
            }
            
            ?>
                </table>

            </div>

        </div>
