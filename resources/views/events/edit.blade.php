
@extends('layouts.app')

@section('title', 'Edit Deatils for' . $event->name)

@section('content')

<div class="row">
	<div class="col-12">

<h1>Edit Details For {{ $event->name }}</h1>

	</div>
</div>


<div class="row">
	<div class="col-12">

	<form action="/events/{{ $event->id }}" method="POST">

		@method('PATCH') <!--This is how laravel gets around http forms only allowing GET & POST // PATCH means youre editing things-->

		@include('events.form')

		<button type="submit" class="btn btn-primary">Save event</button>


	</form>

	</div>
</div>

	
	<hr>

	
@endsection