
@extends('layouts.app')

@section('title', 'events')

@section('content')

<div class="row">
	<div class="col-12">

<h1>Events</h1>

	</div>
</div>


<div class="row">
	<div class="col-12">

	<form action="events" method="POST">

		
		<div class="input-group pb-2">

			<label for="name">Name</label>
			<input type="text" name="name" value="{{ old('name') }}" class="form-control">
			<div>{{ $errors->first('name') }}</div>

		</div>


		<div class="input-group pb-2">

			<label for="email">Location</label>
			<input type="text" name="location" value="{{ old('location') }}" class="form-control">
			<div>{{ $errors->first('location') }}</div>

		</div>

		<div class="form-group">
			<label for="active">Status</label>
			<select name="active" id="active" class="form-control">
				<option value="" disabled>Select event status</option>
				<option value="1">Work</option>
				<option value="0">Lesiure</option>
			</select>
		</div>


		<div class="form-group">
			<label for="company_id">Venue</label>
			<select name="company_id" id="company_id" class="form-control">
				@foreach ($companies as $company)
					<option value="{{ $company->id }}"> {{ $company->name }} </option>
				@endforeach	

			</select>
		</div>


		<button type="submit" class="btn btn-primary">Add event</button>

		@csrf

	</form>

	</div>
</div>

	
	<hr>

	
<div class="row">
	<div class="col-6">

		<h3>Active Events</h3>
		
		<ul>
		
		@foreach ($activeevents as $activeevent)
			
			<li>{{ $activeevent->name }} <span class="text-muted">({{ $activeevent->company->name }})</span></li>

		@endforeach

	</ul>

	</div>

		<div class="col-6">

		<h3>Inactive Events</h3>

		<ul>
		
		@foreach ($inactiveevents as $inactiveevent)
			
			<li>{{ $inactiveevent->name }} <span class="text-muted">({{ $inactiveevent->company->name }})</span></li>

		@endforeach

	</ul>

	</div>
</div>

<div class="row">
	
	<div class="col-12">
		@foreach($companies as $company)

			<h3>{{ $company->name }}</h3>

			<ul>
				@foreach($company->events as $event)

					<li>{{ $event->name }}</li>

				@endforeach
			</ul>
		
		@endforeach

	</div>
</div>


@endsection