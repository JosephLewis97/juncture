	

@extends('layouts.app')

@section('title', 'Details for ' . $user->name)

@section('content')




<div class="row"> 
	<div class="col-12">

		<h1>Details for {{ $user->name }}</h1>
		<p><a href={{ '/users/'.strval($user->id).'/edit' }}>Edit</a></p>

	</div>
</div>


<div class="row">
	<div class="col-12">

		<p><strong>Name</strong> {{ $user->name }}</p>
		<p><strong>Email</strong> {{ $user->email }}</p>		


	</div>
</div>

<form action={{ '/users/'.strval($user->id) }} method="POST">
		@method('DELETE')
		@csrf

		<button class="btn btn-danger" type="submit">Delete</button>
	</form>

	
	
@endsection