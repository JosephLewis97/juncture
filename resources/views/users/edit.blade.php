@extends('layouts.app')

@section('title', 'Edit Deatils for' . $user->name)

@section('content')

<div class="row">
	<div class="col-12">

<h1>Edit Details For {{ $user->name }}</h1>

	</div>
</div>


<div class="row">
	<div class="col-12">

	<form action="/users/{{ $user->id }}/update" method="POST">
		<!--This is how laravel gets around http forms only allowing GET & POST // PATCH means youre editing things-->

		

		@include('users.form')

		<button type="submit" class="btn btn-primary">Save user</button>


	</form>

	</div>
</div>

	
	<hr>

	
@endsection