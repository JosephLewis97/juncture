<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class event extends Model
{
    
    //nothing is guarded. Will allow mass assigment of anything in database. 
	protected $guarded = [];

    protected $attributes = [

        'active' => 1
    ];
    

    public function getActiveAttribute($attribute){

        return $this->activeOptions()[$attribute];
    }


    public function scopeActive($query){

    	return $query->where('active', 1);
    }


    public function scopeInactive($query){

    	return $query->where('active', 0);
    }


    public function company(){

    	return $this->belongsTo(Company::class);
    }

     public function creator(){

        return $this->belongsTo(User::class);
    }

    public function activeOptions(){

        return [

            1 => 'Work',
            0 => 'Lesiure',
            2 => 'Other',
            
        ];
    }
}
