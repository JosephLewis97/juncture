<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Event;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::query()->get();
        return view('users.index', compact("users"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('/register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
            // get files from form and make sure they are what we want
    $this->validate(request(), [
      'name' => 'required',
      'email' => 'required',
      
    ]);
    // store gethered data to database and create copy of the data
    $user = User::create(request(['name', 'email', 'image_name']));
    // see if the user has uploaded a file inthe form
    if($request->hasFile('image_name')){
      $file = $request->file('image_name'); // get the file
      $extension = $file->getClientOriginalExtension(); // get the extension of the file (.png, .gif, .jpeg)
      $user->image_name = 'user/'.strval($user->id).'.'.$extension; // creating the new image name based off the PK of the game and the file extension
      Storage::disk('public')->put($user->image_name,  File::get($file)); // storing image to "public/storage/game/<filename>"
      $user->save(); // saving filename to database
    }
    return redirect()->to('/home');
  }
    

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    
    public function show(User $user)
    {
        return view('users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
        ]);

        $user->update($request->all());
        return redirect()->to('/users')->with('Success','Your profile was updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        return redirect()->route('users.index')->with('Sucess','User deleted successfully');
    }
}

