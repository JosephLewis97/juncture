<?php

namespace App\Http\Controllers;

use App\Event;
use App\Company;
use App\User;


class EventsController extends Controller
{


    public function __construct(){

        $this->middleware('auth');
    }

    public function index()
    {

        $events = Event::all();


       

	return view('events.index', compact('events', 'location'));
    //return $creatorName;

    }

    public function create(){

        $companies = Company::all();
        $event = new Event();

        return view('events.create', compact('companies', 'event'));
    }

    public function store(Event $event){

    	$data = request()->validate([

    		'name' => 'required|min:3',
    		'location' => 'required',
            'active' => 'required',
            'date' => 'required',
            'creator_id' => 'required'
            

    	]);


        Event::create($data);

    	return redirect('/events');//re-diretcs back to index

    }

    public function show(Event $event){

        
        $creatorName = User::query('user')
        ->select('name')
        ->where('id', $event['creator_id'])
        ->get();
        
        

        return view('events.show', compact('event', 'creatorName'));
        //return $creatorName;

    }



    public function edit(Event $event){

        $companies = Company::all();
        return view('events.edit', compact('event', 'companies'));
    }

    public function update(event $event){

        $event->update($this->validateRequest());
        return redirect('events/' . $event->id);
    }

    public function destroy(Event $event){

        $event->delete();

        return redirect('events');
    }

    private function validateRequest(){

        return request()->validate([

            'name' => 'required|min:3',
            'location' => 'required',
            'active' => 'required',
           
        ]);


    }
}












