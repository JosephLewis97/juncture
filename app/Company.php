<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $guarded = [];

    public function events(){

    	return $this->hasMany(event::class);
    }
}
